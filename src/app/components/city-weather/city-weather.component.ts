import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpHandler } from '@angular/common/http';

@Component({
  selector: 'app-city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.scss'],
  providers: [HttpClient]
})
export class CityWeatherComponent implements OnInit {
  _name: string;

  description: string = "";
  currentTemp: string = "";
  minTemp: string = "";
  maxTemp: string = "";
  windSpeed: string = "";
  icon: string = "";
  lastUpdate: string;


  @Input()
  set cityName(value: string) {
    if (value != undefined) {
      this._name = value;
      this.getWeatherByCity();
    }
  }
  get cityName() {
    return this._name;
  }

  headers: HttpHeaders = new HttpHeaders({ 'Accept': 'application/json;odata=verbose' });
  options = ({ headers: this.headers });


  constructor(public http: HttpClient) { }

  ngOnInit() {
    setTimeout(() => {
      this.getWeatherByCity()
    }, 180000);
  }

  ShowWeatherIcon(): string {
    let url = "http://openweathermap.org/img/w/" + this.icon + ".png";
    return url;
  }

  private getWeatherByCity() {
    let url = "https://api.openweathermap.org/data/2.5/weather?q=" + this.cityName + "&appid=bcade0af4726efc36b07c076473191b4&units=metric"
    let result;

    this.http.get(url, this.options).subscribe(data => {
      result = data;
      if (result.weather.length > 0) {
        this.description = result.weather[0].description;
        this.currentTemp = result.main.temp + "º";
        this.maxTemp = result.main.temp_max + "º";
        this.minTemp = result.main.temp_min + "º";
        this.windSpeed = result.wind.speed;
        this.icon = result.weather[0].icon;
        this.lastUpdate = new Date().toLocaleString();
        this.AddStorage(result);
      }
    },
      error => {
        console.log(error);
      });
  }

    AddStorage(data: object): void {
      let hisotric = [];

      if (localStorage.getItem(this.cityName) === null) {
          hisotric.push(data);
      }else{
          hisotric = JSON.parse(localStorage.getItem(this.cityName));
          hisotric.push(data);
      }

      localStorage.setItem(this.cityName, JSON.stringify(hisotric));
  }

    GetStorage(city: string): object {
      let data = [];

      if(localStorage.getItem(city) != null){
          data = JSON.parse(localStorage.getItem(city));
      }
      return data;
  }
}
