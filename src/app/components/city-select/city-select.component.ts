import { Component, OnInit } from '@angular/core';
import { ICityData } from '../../interfaces/icity-data';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-city-select',
  templateUrl: './city-select.component.html',
  styleUrls: ['./city-select.component.scss']
})
export class CitySelectComponent implements OnInit {

  cities: ICityData[] = [
    { Name: "Santiago", Visible: true }
    , { Name: "Buenos Aires", Visible: true }
    , { Name: "Lima", Visible: true }
    , { Name: "Sao Paulo", Visible: true }
  ];

  constructor() { }

  ngOnInit() {
  }

  ShowHideCity(cityName: string) {
    if (this.cities.find(item => item.Name == cityName)) {
      this.cities.filter(item => item.Name == cityName)[0].Visible = !this.cities.filter(item => item.Name == cityName)[0].Visible;
    }
  }

  GetCityClass(visible: boolean): string {
    if (visible) {
      return "select";
    }
    else {
      return "unselect";
    }

  }
}
